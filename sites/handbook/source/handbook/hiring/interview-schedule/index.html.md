---
layout: handbook-page-toc
title: "Interview Schedule"
description: "Interview Schedule is a program GitLab uses to increase efficiency in scheduling interviews."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Interview Schedule

Resource Guide is integrated with Greenhouse via API.
DRI: Candidate Experience Team

#### How to schedule 1:1 via Interview Schedule
1. In Greenhouse click on “schedule loop” located on the top left hand side of the screen
1. Under “Interviewers” type the name of the person who you need to schedule the interview with
1. Set the duration of the interview, Click “next”, 
1. This will take you to the candidate’s availability. Click “next”
1. Find the interview time that works best and hit “select”
1. Click “Add Zoom to meeting” under each interview
1. Ensure the Interview Calendar is selected on the bottom of the page so the interview gets put on the appropriate calendar
1. Once you see the confirmation message at the top of the screen that the interview invites have been successfully sent, you can close the Interview Schedule tab and go back to Greenhouse
1. Once you refresh Greenhouse, you should see the interviews populated
1. Lastly, you’ll send confirmations via Guide

#### How to schedule technical or behavioral interviews using pools and attributes
1. In Greenhouse click on “schedule loop” located on the top left hand side of the screen
1. Under “Interviewers” type the name of the pool who you need to schedule the interview with (e.g. ruby on rails, golang, support technical, support behavioral)
1. Ensure the duration of the interview is correct, click next
1. Adding the appropriate Attribute filters at the bottom for 2nd attribute (e.g. americas, apac, non-male)
1. Click Next
1. This will take you to the candidate’s availability. Click “next”
1. Find the interview time that works best and hit “select”
1. Click “Add Zoom to meeting” under each interview
1. Ensure the Interview Calendar is selected on the bottom so the interview gets put on the appropriate calendar

#### How to update a pool in Interview Schedule
1. Click on the `Settings` tab at the top of the screen
1. Go down to the Attributes section in the left hand panel and select
1. Hit `View All Attributes`
1. Select the attribute you need to add/remove someone from 
1. In this screen, you can add or remove any person from a specific attribute. 
1. Make sure to hit `Save` when you're done



